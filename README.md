# rph-coding-env

Hello, my name is breezy and this is a coding environment for making profiles on [RPHaven](http://www.rphaven.com/).

By using this environment, you can:

- Store your profile code easily on your computer

- Work on profiles when the website is unavailable

- Edit your code in your text editor of choice, instead of RPH's built-in editing windows.

- Use a live-updating preview while you code

- Write all your code using the templating languages [Pug](https://hackernoon.com/an-introduction-to-pug-1dbe7cfcacd8) and [SCSS](https://sass-lang.com/guide), which add tons of extra functionality to standard HTML and CSS, respectively. (This is the important bit!)

Making profiles is really fun, but assembling everything using RPH's built-in editor can often be a chore. This coding environment aims to get rid of all the wonky parts of profile creation, so you can focus on the fun ones.

## How To Use the Environment
First, you should have the following tools installed. If you are at all interested in web development these are very standard tools, so don't be hesitant to download them.

- [Git](https://git-scm.com/)

- [Node](https://nodejs.org/en/)

You should also know how to [open a command line at a specific folder](https://www.youtube.com/watch?v=YdDngaoD1WE).

Once you have Git and Node installed, open a command line in the folder you want to install your environment in. You probably want an environment for each profile you create, so make a new folder each time.

In the command line you opened for your folder, run these commands:

This one downloads the environment from the internet.
```
git clone https://breezystatic77@bitbucket.org/breezystatic77/rph-coding-env.git
```

This one installs all the node packages the environment requires.
```
npm install
```

Finally, this one starts the environment up!
```
npm run environment
```

The folder should now look something like this:
```
- env
	- (a bunch of files you shouldn't worry about)
	+ compiled
		- compiled.css
		- compiled.html
		- compiled.js
	+ server
		- (a bunch of files you shouldn't edit)
	+ source
		- main.js
		- main.pug
		- main.scss

```

The only 3 files you should edit are

- `env/source/main.js`

- `env/source/main.pug`

- `env/source/main.scss`

While the environment is running, it will:

- Compile those 3 files, and display them in a live preview that opens automatically in your browser.

- Turn those files into code you can copy and paste into RPH.

	- That code is placed in the 3 files in the `compiled` folder.
	
	- Copy the contents of `compiled.css`, `compiled.html`, and `compiled.js` into their respective spots in the RPH profile editor. *Do not edit files in `/env/compiled` directly!*

This is a work in progress. I have tried to make the live preview reflect the actual RPH wrapper code as much as possible, but there might be some discrepancies. Please report any bugs [here](https://bitbucket.org/breezystatic77/rph-coding-env/issues). Include pastebins for your `env/source` directory files, and a link to the RPH profile the bug is effecting.

An example profile can be found [here](https://bitbucket.org/breezystatic77/rce-example/), to see what a fully-coded profile using Pug, SCSS, and Javascript looks like.

Thanks, and [have fun](https://www.youtube.com/watch?v=qhyuPr9ULqI).