const liveServer = require('live-server');
const gulp = require('gulp');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const gutil = require('gulp-util');
const rename = require('gulp-rename');
const createFile = require('create-file');

createFile('./env/source/main.js', '//main.js', (err) => {
	if (err) {
		console.log(err);
	}
});

createFile('./env/source/main.scss', '//main.scss', (err) => {
	if (err) {
		console.log(err);
	}
});

createFile('./env/source/main.pug', '//main.pug', (err) => {
	if (err) {
		console.log(err);
	}
});

gulp.watch('env/source/*.scss', { ignoreInitial: false }, (cb) => {
	gulp.src([
		'./env/source/main.scss'
	])
		.pipe(sass().on('error', gutil.log))
		.pipe(rename('compiled.css'))
		.pipe(gulp.dest('./env/compiled'));
	cb();
});

gulp.watch('./env/source/*.js', { ignoreInitial: false }, (cb) => {
	gulp.src([
		'./env/source/main.js'
	])
		.pipe(rename('compiled.js'))
		.pipe(gulp.dest('./env/compiled'));
	cb();
});

gulp.watch('env/source/*.pug', { ignoreInitial: false }, (cb) => {
	gulp.src([
		'./env/source/main.pug'
	])
		.pipe(pug({ pretty: true }).on('error', gutil.log))
		.pipe(rename('compiled.html'))
		.pipe(gulp.dest('./env/compiled'));
	
	gulp.src([
		'./env/server/profile.pug'
	])
		.pipe(pug({ pretty: true }).on('error', gutil.log))
		.pipe(gulp.dest('./env/server'))
	cb();
});

liveServer.start({
	port: 6969,
	root: "./env/server",
	ignore: "env/source",
	mount: [["/compiled", "./env/compiled"]]
});